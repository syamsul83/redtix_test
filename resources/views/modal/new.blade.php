<form action="{{ route('user.store') }}" id="frm-user" name="frm_user" role="form" method="post">
    <div class="form-group">
        <label for="input-email">Email</label>
        <input type="email" class="form-control" id="input-email" name="email" placeholder="Enter email" required>
    </div>
    <div class="form-group">
        <label for="input-first-name">First Name</label>
        <input type="text" class="form-control" id="input-first-name" name="first_name" placeholder="Enter first name" required>
    </div>
    <div class="form-group">
        <label for="input-last-name">Last Name</label>
        <input type="text" class="form-control" id="input-last-name" name="last_name" placeholder="Enter last name" required>
    </div>
    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" name="active" value="1" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
                Active
            </label>
        </div>
    </div>
</form>


