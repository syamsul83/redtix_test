<form action="{{ route('user.update', ['id' => $user_data->id]) }}" id="frm-user" name="frm_user" role="form" method="post">
    <div class="form-group">
        <label for="input-email">Email</label>
        <input type="hidden" id="method_put" name="_method" value="put">
        <input type="email" class="form-control" id="input-email" name="email" placeholder="Enter email" required value="{{ $user_data->email }}">
    </div>
    <div class="form-group">
        <label for="input-first-name">First Name</label>
        <input type="text" class="form-control" id="input-first-name" name="first_name" placeholder="Enter first name" required value="{{ $user_data->first_name }}">
    </div>
    <div class="form-group">
        <label for="input-last-name">Last Name</label>
        <input type="text" class="form-control" id="input-last-name" name="last_name" placeholder="Enter last name" required value="{{ $user_data->last_name }}">
    </div>
    <div class="form-group">
        <div class="form-check">
            <input class="form-check-input" name="active" value="1" @if($user_data->active == 1)checked @endif type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
                Active
            </label>
        </div>
    </div>
</form>
