<!doctype html>
<html lang="en">
    <head>
        <title>RedTix Test :: User</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
        <link href="{{ asset('node_modules/sweetalert2/dist/sweetalert2.css') }}" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>User List</h2>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <h3>Filter</h3>
                    <form name="filter-frm" id="frm-filter">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="input-email">Email</label>
                                <input type="text" class="form-control" id="input-email" name="email" placeholder="Search email" >
                            </div>
                            <div class="form-group col-md-4">
                                <label for="input-status">Status</label>
                                <select id="input-status" name="status" class="form-control">
                                    <option value="">-- All Status --</option>
                                    <option value="1">Active</option>
                                    <option value="0">Not Active</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="input-name">Name</label>
                                <input type="text" class="form-control" id="input-name" name="name" placeholder="Search Name" >
                            </div>
                        </div>
                        <div class="form-row text-right">
                            <div class="form-group col-md-12">
                            <button type="button" id="search-form" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                <hr/>
                <div class="col-md-12">
                    <table id="user-table" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="main-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="submit-form" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
{{--    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('node_modules/sweetalert2/dist/sweetalert2.all.js') }}"></script>
    <script type="text/javascript">

        var popup_modal = function (type, id) {
            $('#main-modal .modal-body').empty();
            $('#main-modal').modal();

            var url = '';
            switch(type) {
                case 'new' :
                    $("#modal-title").html("New User");
                    url = '{{ route('user.create') }}';
                    break;
                case 'edit' :
                    $("#modal-title").html("Edit User");
                    url = '{{ route('user.show', ['user' => null]) }}/' + id;
                    break;
            }

            fetch (url, {
                method: 'GET',
                credentials: 'include'
            })
                .then((response) => {
                    if (!response.ok || response.status > 299) {
                        return response.json();
                    }
                    return response.text();
                })
                .then((response) => {
                    if (response.constructor === {}.constructor) {
                        alert(response.description);
                        return false;
                    }
                    $('#main-modal .modal-body').html(response);
                });
            return true;
        };

        $(function () {
            var data_table = $("#user-table").DataTable(
                {
                    deferRender: true,
                    fixedHeader: false,
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{ route('user.list.data') }}",
                        type: "POST",
                        data: function(d){
                            d.form = $('form#frm-filter').serializeArray();
                        },
                    },
                    "columns": [
                        {"data" : "id"},
                        {"data" : "email"},
                        {"data" : "first_name"},
                        {"data" : "last_name"},
                        // {"data" : "email"},
                        {"data" : "active"},
                        {
                            "data" : "id",
                            "render" : function (data, type) {
                                return '   <button class="btn btn-outline-dark" data-type="edit">\n' +
                                    '        Edit\n' +
                                    '   </button>' +
                                    '<button class="btn btn-outline-danger" data-type="delete">\n' +
                                    '        Delete\n' +
                                    '   </button>';
                            }
                        }
                    ],
                    "dom": '<"top"<"ctrl"B>>rt<"bottom"ip><"clear">',
                    "buttons": [
                        "pageLength",
                        {
                            text: 'Create New',
                            className: 'create_new',
                            action: function ( e, dt, node, config ) {
                                return popup_modal('new', 0);
                            }
                        }
                    ],
                    "ordering": false
                }
            );

            $('#user-table tbody').on( 'click', 'button', async function (data) {
                var type = $(this).data('type');
                var datarow = data_table.row( $(this).parents('tr') ).data();
                if (type === "edit") {
                    return popup_modal(type, datarow.id);
                }

                if (type === 'delete') {

                    Swal.fire({
                        title: 'Delete User',
                        text: "Are you sure to delete this user?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            fetch('{{ route('user.destroy', ['id' => null]) }}/' + datarow.id, {
                                method: 'DELETE',
                                credentials: 'include',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    'Accept': 'application/json',
                                    // "Content-Type": "x-www-form-urlencoded"
                                }
                            })
                                .then((response) => response.json())
                                .then((response) => {
                                    if (response.error) {
                                        Swal.fire("Error", response.description, "error");
                                    } else {
                                        Swal.fire("Success", response.description, "success");

                                        Swal.fire({
                                            title: response.error ? "Error" : "Success",
                                            text: response.description,
                                            type: response.error ? "error" : "success"
                                        }).then(function() {
                                            data_table.ajax.reload();
                                        });
                                    }
                                }).catch((e) => {
                                Swal.fire("Error", "Internal Server Error", "error");
                                console.log(e);
                            });
                        }
                    });

                }
            });

            $("#search-form").on("click", function () {
                data_table.ajax.reload();
            });

            $("#submit-form").on("click", async function(){
                let form = $('.modal-body form').serialize();
                console.log(form);
                let formData = new FormData();
                formData.append('form', form);
                if ($("#method_put").length) {
                    formData.append('_method', 'put');
                }

                fetch($(".modal-body form").attr('action'), {
                    method: $(".modal-body form").attr('method'),
                    credentials: 'include',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Accept': 'application/json',
                        // "Content-Type": "x-www-form-urlencoded"
                    },
                    body: formData
                })
                    .then((response) => response.json())
                    .then((response) => {
                        if (response.error) {
                            Swal.fire("Error", response.description, "error");
                        } else {
                            Swal.fire("Success", response.description, "success");

                            Swal.fire({
                                title: response.error ? "Error" : "Success",
                                text: response.description,
                                type: response.error ? "error" : "success"
                            }).then(function() {
                                $('#main-modal').modal('hide');
                                data_table.ajax.reload();
                            });
                        }
                    }).catch((e) => {
                    Swal.fire("Error", "Internal Server Error", "error");
                    console.log(e);
                });
            });
        });
    </script>
</html>
