<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('user');
});

Route::resource('user', 'UserController');
//Route::get('user', ['as' => 'user', 'uses' => 'UserController@index']);
Route::post('user/list_data', ['as' => 'user.list.data', 'uses' => 'UserController@list_data']);
