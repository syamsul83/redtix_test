<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

REDTIX TEST
========================



Requirement
-------------
- Composer 
- npm


Installation
--------------

Here is the step to do install This Application.

- Clone it from GIT
- Copy .env.dist to .env
- Set necessary Variable in .env except for APP_KEY
```sh
$ cp .env.dist .env
```

- Install necessary vendor using composer.
```sh
$ composer install
```
- Install necessary node_modules.
```sh
$ npm install
```
- Generate App Key
```shell script
$ php artisan key:generate
```
- Create Database, migrate new tables and load default information.
- Manually create the Database based on the .env file
- Run Migration (Will create the table for this test)
```sh 
$ php artisan migrate
```

- Refresh Cache 
```sh 
$ php artisan cache:clear
```

