<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modal/new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $return = [
                'error' => false,
                'description' => ''
            ];

            $form_str = filter_var($request->get('form', null));

            parse_str($form_str, $data_form);
            $validator = Validator::make($data_form, [
                'email' => 'required|email|unique:user',
                'first_name' => 'required',
                'last_name' => 'required'
            ]);

            if ($validator->fails()) {
                throw new \ErrorException($validator->errors()->first());
            }

            $user = new User();
            $user->email = $data_form['email'];
            $user->first_name = $data_form['first_name'];
            $user->last_name = $data_form['last_name'];
            $user->active = isset($data_form['active']) ? 1 : 0;

            $return['description'] = 'Successfully inserted new user!';

            if(!$user->save()) {
                throw new \ErrorException('Failed to create new user!');
            }
        }
        catch (\Exception $e) {
            $return['error'] = true;
            $return['description'] = $e->getMessage();
        }
        finally {
            return new JsonResponse($return);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function show($user = null)
    {
        //
        $user_data = User::find($user);
        return view('modal/show')->with(compact(['user_data']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $return = [
                'error' => false,
                'description' => ''
            ];

            $form_str = filter_var($request->get('form', null));

            parse_str($form_str, $data_form);

            $validator = Validator::make($data_form, [
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required'
            ]);

            if ($validator->fails()) {
                throw new \ErrorException($validator->errors()->first());
            }

            $user = User::find($id);
            if (!$user) {
                throw new \ErrorException('Invalid User!');
            }

            $user->email = $data_form['email'];
            $user->first_name = $data_form['first_name'];
            $user->last_name = $data_form['last_name'];
            $user->active = isset($data_form['active']) ? 1 : 0;

            $return['description'] = 'Successfully update user!';

            if(!$user->save()) {
                throw new \ErrorException('Failed to update user!');
            }
        }
        catch (\Exception $e) {
            $return['error'] = true;
            $return['description'] = $e->getMessage();
        }
        finally {
            return new JsonResponse($return);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id = null)
    {
        try {
            $return = [
                'error' => false,
                'description' => ''
            ];

            $user = User::find($id);
            if (!$user) {
                throw new \ErrorException('Invalid User!');
            }

            $return['description'] = 'Successfully delete user!';

            if(!$user->delete()) {
                throw new \ErrorException('Failed to delete user!');
            }
        }
        catch (\Exception $e) {
            $return['error'] = true;
            $return['description'] = $e->getMessage();
        }
        finally {
            return new JsonResponse($return);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list_data(Request $request) {
        $data = [];

        $users = DB::table('user');

        $data_form = $request->request->get('form', 'null');

        $form = [];
        foreach ($data_form as $new_data) {
            $form[$new_data['name']] = $new_data['value'];
        }

        if (isset($form['email']) && !empty($form['email'])) {
            $users->where('user.email', 'like', '%' . trim($form['email']) . '%');
        }

        if (isset($form['name']) && !empty($form['name'])) {
            $value_search = trim($form['name']);

            $users->where(function($q) use ($value_search){
                $q->where('user.first_name', 'like', '%' . $value_search . '%')
                    ->orWhere('user.last_name', 'like', '%' . $value_search . '%');
            });
        }

        if (isset($form['status']) && $form['status'] !== '') {
            $users->where('user.active', '=', $form['status']);
        }

        $result = $users->count();

        $users->offset($request->get('start'))->limit($request->get('length'));

        $data = [
            'draw' => $request->get('draw'),
            'recordsTotal' => $result,
            'recordsFiltered' => $result
        ];

        $data['form'] = $form;

        $data['data'] = [];
        $results = $users->get();
        foreach ($results as $result) {
            $data['data'][] = [
                'id' => $result->id,
                'email' => $result->email,
                'first_name' => $result->first_name,
                'last_name' => $result->last_name,
                'active' => $result->active ? "Yes" : "No"
            ];
        }

        return new JsonResponse($data);
    }
}
